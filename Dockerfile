FROM node:19.0.1-bullseye
WORKDIR /home/node/app
COPY . .
RUN npm install
EXPOSE 8000
CMD npm start
